# Who are you and what is this?

As this community and some people I met told me, it doesn't care who you are but what you think, how you think it and why.
I believe that many people will dislike this letter or not even see the needs for us to make such drastic changes in the structure of the Monero organization as a whole, but the recent [CCS robbery event](https://monero.observer/luigi-discloses-critical-ccs-wallet-breach/) should have opened your eyes a little bit.

This is a letter to push some needed changes to the general environment, and hopefully to make sure incidents like the one we seen in the last days never happen again any time soon.

## Main points of change 

- Code hosting

- Community hangouts and conversation places

- CCS funding and security

- Wallets and OSes

- Exchanges and swaps

- Extra points

---

These 5 main points must change. We can't keep covering our eyes or depending on untrustable humans, that could even make mistakes by their own.

As I want to prove my points really hard this open letter will have sources and links attached to it, just to make sure that there is will by the current core team to apply these changes, they don't have to put much effort in further researching some topics they know nothing about, we are not all perfect and I can understand that as I will never be too.

```
"Great losses are great lessons."
```
-- Amit Kalantri, Wealth of Words

### 1- Code hosting 

This is one of the most obvious and ridicolous points in the whole letter.
As we all know by the official links provided on the [getmonero.org website](https:/getmonero.org), the official source code for the Monero Crypto Currency is hosted on [GitHub](https://github.com).

You might be wondering
> What's the issue with that? Many Open Source organizations use GitHub.

First of all GitHub code hosting platform is [owned and maintained](https://news.microsoft.com/announcement/microsoft-acquires-github/) by Microsoft, just one of the biggest tech corporations alive today.
GitHub is anti free-speech and open code publication, as it happened in the case of the well known [Tornado.cash case](https://cointelegraph.com/news/tornado-cash-co-founder-reports-being-kicked-off-github-as-industry-reacts-to-sanctions).

So, why should the most hunted Crypto Currency, which is interesting enough for feds to burn a [huge amount of tax money to get a tracking tool](https://cointelegraph.com/news/the-irs-offers-a-625-000-bounty-to-anyone-who-can-break-monero-and-lightning), host its source code on such platform ruled with censorship and tyranny?

There were some failed attemps from some community members to create a [Tor archive](https://ccs.getmonero.org/proposals/Archive-monero-code-in-uncensorable-way.html) for the Monero code, but we should more care on having a secure primary source, and later carying about adding mirrors and archives to keep it 100% unstoppable.

### 2- Community hangouts and conversation places

This is one of the biggest issues that really takes the word away from the majority of the community and just doesn't make sense at all.

Why would the Core team of Monero hang out in a IRC such as [Libera](https://libera.chat/), or even on [Matrix](https://matrix.org/)? (at least the monero.social Matrix server is self-hosted)
Do you really care this little about your OPsec considering what past events, like the arrest of one of the [Tornado.cash Founder](https://www.coindesk.com/policy/2023/08/23/tornado-cash-devs-arrested-on-money-laundering-sanctions-violation-grounds-over-alleged-1b-moved/), have brought to our eyes?

Let's dispatch everything down in details. 

Why is Libera IRC not ideal for Monero?
> It even has a Tor mirror!

To join the channels related to Monero, you need to have an account registered on the server, and this is fine, even if an email is required (but just for account recovery purposes).
The real point that makes no sense is their [SASL IP filtering](https://libera.chat/guides/sasl#sasl-access-only-ip-ranges).

Basically what they recommend is to disable your proxy and let an account on their IRC to be tied to your ISP's IP and your real identity just to chat.

This measure is to do do what? Mitigate spam? Yes of course, and also Policy Violations, as stated on their page in these two paragraphs

> If you are using a VPN or other anonymization service, consider temporarily disabling it, connecting to Libera.Chat, and only re-enabling it after you have created and verified the account.

> SASL-only restrictions are typically applied to address ranges that are the source of frequent policy violations due to providing easy access to dynamic addresses to a wide range of users. These ranges are typically used by VPN, cloud-computing, and mobile network providers. These restrictions are not targeted at individual users.

How to solve this? 

Switch to [OFTC network](https://www.oftc.net/), like your fork Wownero did, which allows Tor and doesn't block your connections to mitigate 'spam' or to preserve their 'policy', or just self-host a server. We can raise the funds for it and for your work!

And what about Matrix? 

[Matrix could be replaced by XMPP without any look back](https://lukesmith.xyz/articles/matrix-vs-xmpp/) as it is: spooky, insecure (built on [WebRTC](https://github.com/matrix-org/webrtc), doesn't support even a basic HTTP proxy) and has horribly bloated clients like Element.
But most importnantly, it's made by a company affiliated with the Israeli intelligence!

WebRTC is very [leaky](https://dzone.com/articles/webrtc-security-vulnerabilities-you-should-know-ab) , is the base for highly sensitive stuff like [End To Encryption](https://webrtchacks.com/how-does-webrtc-end-to-end-encryption-work-matrix-org-example-dave-baker/) and doesn't support proxies by nature, this is why it's disabled in the [Tor Browser for example](https://tor.stackexchange.com/questions/876/does-tor-work-with-webrtc#1070). VPNs won't be a good solution either as you can't never fully trust a provider that isn't yourself and it will even then not mask your IP on Matrix because of [webrtc](https://proprivacy.com/vpn/guides/the-webrtc-vpn-bug-and-how-to-fix-it) and it cannot be disabled on matrix in any way.

Credits to the people behind the kinda failed attempt of providing an XMPP server to the community, as it's unstable and buggy.
This can be easily solved with some dedication and maybe a few more people working together.

### 3- CCS funding and security

This topic is very fresh and on the mouth of many. We can all agree Luigi's Infosec and OPsec are terrible and just giving the custody of the CCS wallet to someone else is not going to give the funds a 100% guarantee of not being stolen again.

What could be done?

Use MultiSig, yes MultiSig. In particular [2/3 MultiSig](https://monerodocs.org/multisignature/).
With one key shared between the Core team wallet, one with the CCS beneficial and one by the coordinator(?).

Would this solve every issue and any potential future wallet drainings?

In my opinion, yes it would.
For each different CCS a new wallet should be created to not let sums like 2,675.73 XMR stay in the same place all together, becoming much more interesting to steal.
MultiSig will prevent arbitrary wallet drainings even if using a hot wallet server side.

The real question is, is this very hard to implement? 

There are darknet marketplaces which enable the users to transact with MultiSig , such as DarkMatterMarket (won't link for security reasons, but you can always look it up on dread and confirm this information by yourself).
If a DNM did it, our core can do it as well for the CCS funding, which is an essential service of the whole Monero ecosystem, if not the most important one for its future thru the funding of new features and related websites and resources.

### 4- Wallets and OSes

This is much more of a personal take and opinion, widely shared around the community but that should be handled with the right attention.

For which operating systems are Monero wallet available?

Too many, 90% of them are proprietary and expose the users to a very high risk no matter what precautions are taken.
As ["Monero takes privacy very seriously"](https://www.getmonero.org/resources/about/), such OSes shouldn't be officially supported to not put in danger the new users or unexperienced users approaching to it. 

Windows and MacOS MUST be removed as officially supported operating systems.

The same can be applied for iOS ([jailbreak doesn't help](https://support.apple.com/guide/iphone/unauthorized-modification-of-ios-iph9385bb26a/ios)), belonging to Apple, but not for Android, as it has privacy respecting (no root required and with the option to relock the bootloader) ROMs like [Graphene OS](https://grapheneos.org/), [Calyx OS](https://calyxos.org/) and other with a wider list of supported devices.

Why?

Because transacting anonymously and privately on a OS that has [telemetry](https://www.independent.co.uk/tech/windows-10-users-banned-from-torrenting-by-piracy-sites-because-of-worries-that-microsoft-spies-on-downloads-10468903.html) ([more](https://winbuzzer.com/2021/04/02/report-apple-and-google-continue-taking-telemetry-data-even-when-users-opt-out-xcxwbn/)) and built-in spywares in its core it's just not a good idea.
Also considering that such OSes are aiming to become [cloud based](https://analyticsdrift.com/microsoft-plans-to-move-windows-completely-to-the-cloud/), this will go completely against the "Not your Keys Not your Coins" philosophy that the Core and the community is trying to follow, for example,  by not adding [closed source or proprietary wallets](https://github.com/monero-project/monero-site#general-change-recommendations) in getmonero.org's wallet section as stated in this paragraphs

> After a discussion, the community decided to include only open source wallets in the 'Downloads' section of the website. Requests to add closed source wallets to that page will be rejected.

Closed source OSes should deserve the same treatment, but apparently privacy, data security and freedom is not at first place compared to marketcap and FIAT value.

([still listing Ledger which is mainly closed source as well](https://www.getmonero.org/downloads/#hardware))

Same thing should be applied for MyMonero, Edge and [Cake wallet](https://github.com/monero-project/monero-site/pull/2013). By being companies their only goal is profit, they don't care about us and will sell if something happen, we should count only on OUR forces to keep the development running.

MyMonero and Edge are two particular cases as they are openly listed under a section named as "Remote synchronization (lightweight)" which is described as 

> These wallets share your private view key with a remote server, which continuously scans the blockchain looking for your transactions. They are faster to use, but your privacy can be lessened if you don't control the remote server.

These wallets MUST be removed from the official website as soon as possible without any look back. Even with this disclaimer.

We can stop them from producing software?

No we cant as this is an open landscape, but at least do not give them visibility obviously going against the ["values"](https://www.getmonero.org/resources/about/) stated 

> Our Values

>> Monero is more than just a technology. It's also what the technology stands for. Some of the important guiding philosophies are listed below.

>> Security:
>> Users must be able to trust Monero with their transactions, without risk of error or attack. Monero gives the full block reward to the miners, who are the most critical members of the network who provide this security. Transactions are cryptographically secure using the latest and most resilient encryption tools available.

>> Privacy:
>> Monero takes privacy seriously. Monero needs to be able to protect users in a court of law and, in extreme cases, from the death penalty. This level of privacy must be completely accessible to all users, whether they are technologically competent or have no idea how Monero works. A user needs to confidently trust Monero in a way that this person does not feel pressured into changing their spending habits for risk of others finding out.

>> Decentralization:
>> Monero is committed to providing the highest degree of decentralization in both network security and code development. Its Proof of Work algorithm prevents specialized mining hardware from dominating the network and allows for fair distribution of block rewards. Additionally, Monero's development and research are conducted via global collaboration, and the project is carried out with utmost transparency. Each development decision is open to public discussion, and every major developer meeting is published online.


### 5- Exchanges and swaps

This last point is centered on the '[Merchants & Exchanges](https://www.getmonero.org/community/merchants/#exchanges)' section of getmonero.org, with a focus on the 'Centralized exchanges (CEXs) & Swappers' sub section.

These exchanges are described as 

> If you prefer to use centralized exchanges, here is a list of renowned CEXes and swappers. Many more exchanges support Monero, we list here only a few reputable ones.

Nothing about KYC? Nothing about protecting your community about it?

As this is the primary source of information of the coin, I'm very disappointed and do not understand the decision making behind it.

You might be asking

> Whats the Issue with KYC?

KYC, known as 'Know You Customer', is a way of authenticating customers based on their identification documents and private data. Such as name, address, phone number and picture of a passport or driving license.

Same thing discussed for closed source wallets and the bad sides of giving them visibility, even with a disclaimer, this MUST be removed as soon as possible and only list valuable options that really follow your 'Values'.

The practice to KYC is a risk, and even if Monero is untraceable, these entities shouldn't be trusted even as primary sources to buy with FIAT. 
They still know who you are, how much Monero you bought and are thanks to the continues regulation they have to cooperate with and disclose that info to the tax authority. The Tax Authority could make users trouble if they find out that you bought Monero for whatever reason, putting these users under a severe threat, especially for newcomers.


### 6- Extra points

Instead of using something like Reddit, Lemmy or Twitter to discuss about our favorite coin, which are full of Javascript, soft KYCed or owned by a company, why not bringing back to life the old good [forum](https://forum.getmonero.org/)?

It's there, online for all of us to use, even more safely if mirrored on Tor or I2P! 

Might need a refresh, but everything can be worked on by anybody as we can all be contributors or maintainers.

Wownero stands out as they have [their own forum](https://forum.wownero.com/) backed by solid [Simple Machine](https://www.simplemachines.org/) and do not soffer as much problems in the last points as well.


---

### Final thoughts

I really hope this letter gets discussed and makes a change in your minds and in your priorities as a community, as well as bringing real change and solving the issues described in it, if you really value and vouch privacy, financial freedom, security and anonymity you just CAN'T ignore this letter at all.

If you think it this way FIGHT back, we can't let an important tool as Monero rotten to the bone like everything else in this world.

I hope you guys will take my critics seriously, as it took me a lot of hassle to set this account as well.

```
"Live free or fucking die."
```
-- J. Stark (RIP)


### THE END - Taking action

[As GitHub censored this letter as issue in the meta repository of the official monero-project](https://github.com/monero-project/meta/issues/919), even if I can see it from my account and this account has not been notified of anything, nor even the 'shadow ban' of said issue, I sent it to [monero.observer](https://monero.observer) and also uploaded it to a [codeberg repo]().

Expect to see issue and pull requests of every single point of this letter, I'm not just talking, I want to fight as I suggest you to do too.

Do you want to take action? Check the [pull request or issue section](https://github.com/monero-project/monero-site) of getmonero.org and submit the changes you found in line with your own thought, or help with new ones.